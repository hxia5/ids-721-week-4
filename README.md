# IDS 721 Week 3

[![pipeline status](https://gitlab.com/hxia5/ids-721-week-4/badges/main/pipeline.svg)](https://gitlab.com/hxia5/ids-721-week-4/-/commits/main)

## Overview
* This is my repository ofIDS 721 Mini-Project 4 - Containerize a Rust Actix Web Service. 

## Purpose
- Containerize simple Rust Actix web app
- Build Docker image
- Run container locally

## Key Steps

1. Create a new repository locally using `cargo new <project name>`.

2. Add necessary dependencies to `Cargo.toml` file.

3. In `main.rs`, write the code for a simple web app, mine is to roll a dice.

4. Create a `Dockerfile` and write the necessary commands to build the image.

5. Build the Docker image using `docker build -t <image name> .`.

6. Run the container using `docker run -p 8080:8080 <image name>`.

7. Go to `localhost:8080` and check if the web app is running.

8. Create `.yml` file and `Makefile` to automate the process.

9. Create a new repository in GitLab, must be empty (without README.md file). 

10. Push the code to the repository in gitlab.

11. Go to the GitLab repository, click `CI/CD`, then check if the pipeline is passed.

##  Screenshot for the web and Docker

- web app

![alt text](<web app.png>)

- Docker image

![alt text](image.png)

- Docker container

![alt text](container.png)

## References

https://actix.rs/docs/getting-started/

