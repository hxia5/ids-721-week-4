use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use rand::Rng;

use serde_json::json;

async fn index() -> impl Responder {
    HttpResponse::Ok().content_type("text/html").body(include_str!("../static/index.html"))
}

async fn roll_dice() -> impl Responder {
    let result: u8 = rand::thread_rng().gen_range(1..=6);
    HttpResponse::Ok().json(json!({ "You roll a": result }))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            //.service(web::resource("/").route(web::get().to(index)))
            //.service(web::resource("/roll").route(web::get().to(roll_dice)))
            //.service(fs::Files::new("/static", "static").index_file("index.html"))
            .service(web::resource("/roll_dice").route(web::get().to(roll_dice)))
            .route("/", web::get().to(index))
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}






